import yargs from "yargs";
import { prompt } from "enquirer";
import path from "path";

import { generate as twitchGenerate } from "./generators/twitch";

const generators: {
  [key: string]: {
    description: string;
    generate: (
      outFile: string,
      manualEvaluationSaveFile: string | null,
      manualEvaluationFile: null | string,
      skipValidation: boolean
    ) => void | Promise<void>;
  };
} = {
  twitch: {
    description: "",
    generate: twitchGenerate,
  },
};

yargs
  .usage("Usage: $0 <command> [options]")
  .help("h")
  .alias("h", ["help", "?"])
  .showHelpOnFail(true)
  .recommendCommands()
  .wrap(100)
  .command(
    "$0",
    "Generate mocks for APIs using predefinded generators",
    () => {},
    ({ $0 }) => console.log(`Use ${$0} --help to show help.`)
  )
  .command(
    ["generate"],
    "Generate api descriptions using the included generators",
    (yargs) => {
      yargs
        .usage("Usage: $0 generate <command> -o [file] [options]")
        .option("outFile", {
          alias: ["o", "out"],
          nargs: 1,
          describe: "output file name",
          demandOption: true,
        })
        .option("saveFile", {
          alias: ["s", "sF"],
          nargs: 1,
          describe: "Save manual evaluation results to this file",
          demandOption: false,
        })
        .option("loadFile", {
          alias: ["l", "lF"],
          nargs: 1,
          describe: "Load manual evaluation results from this file",
          demandOption: false,
        })
        .option("skipValidation", {
          alias: ["y", "yes"],
          describe: "Skip validation of manual evaluation results",
          demandOption: false,
        });
      Object.keys(generators).forEach((key) => {
        yargs.command(
          key.toLowerCase(),
          generators[key].description,
          (yargs) => {
            yargs.usage(
              `Usage: $0 generate ${key.toLowerCase()}  -o [file] [options]`
            );
          },
          (argv) => generateGenerator(key, argv)
        );
      });
    },
    generate
  )
  .command("typescript", "convert spec to typescript helper files").argv;

async function generate(argv: any) {
  const answers: { generator: string } = await prompt({
    message: "Select a generator",
    choices: Object.keys(generators),
    name: "generator",
    type: "select",
  });
  await generateGenerator(answers.generator, argv);
}

async function generateGenerator(generator: string, argv: any) {
  const parsed: {
    root?: string;
    dir?: string;
    base?: string;
    ext?: string;
    name?: string;
  } = path.parse(argv.outFile);
  if (parsed.root === "") parsed.root = "./";
  parsed.ext = ".mckspec";
  parsed.base = undefined;
  await generators[generator].generate(
    path.resolve(path.format(parsed)),
    argv.saveFile,
    argv.loadFile,
    argv.skipValidation != null
  );
}
