import fs from "fs";

export async function startCooldown(cooldown: number) {
  const timefile = fs.existsSync("./timestamp.json")
    ? JSON.parse((await fs.promises.readFile("./timestamp.json")).toString())
    : null;
  const timestamp = timefile != null ? new Date(Date.parse(timefile)) : null;
  console.log(
    `Waiting for start cooldown.... (${
      ((timestamp?.getTime() ?? Date.now()) - Date.now()) / 1000
    } Seconds)`
  );
  if (timestamp != null && timestamp.getTime() - Date.now() > 0)
    await new Promise((r) => setTimeout(r, timestamp.getTime() - Date.now()));
  console.log("Finished.");
  const newTimestamp = new Date(Date.now());
  newTimestamp.setTime(newTimestamp.getTime() + cooldown);
  await fs.promises.writeFile(
    "./timestamp.json",
    JSON.stringify(newTimestamp.toISOString())
  );
}
