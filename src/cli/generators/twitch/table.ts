import puppeteer from "puppeteer";
import {
  ManualEvaluationPlaceholder,
  ManualEvaluationResolver,
} from "../../manualEvaluation";

type ReturnType<T extends keyof object> = {
  hasOtherElements: boolean;
  otherElements?: Element[];
  results:
    | ManualEvaluationPlaceholder<"evaluateTableParams">
    | (
        | { [key in T]: string }
        | ManualEvaluationPlaceholder<"evaluateTableRow">
      )[];
};

export async function extractData<T extends keyof object>(
  containerId: string,
  elementHeaders: { headers: string[]; defaults: { [key in T]: string } }[],
  resultFormat: { [key in T]: string[] }
): Promise<ReturnType<T>> {
  const container: Element | null = document.getElementById(containerId);
  if (container == null) throw new Error("Container not found");
  const leftoverElements: Element[] = Array.from(container.children).filter(
    (el) =>
      el.tagName !== "TABLE" &&
      (el.textContent == null || el.textContent.trim() !== "")
  );
  const allKeys = Object.values<string[]>(resultFormat).reduce(
    (a, b) => [...a, ...b],
    []
  );
  const elements = elementHeaders.map((el) => {});
}

export const evaluateTableParams: ManualEvaluationResolver<
  { [key: string]: string } | ManualEvaluationPlaceholder<"evaluateTableRow">,
  "evaluateTableParams",
  {
    resultFormatAdditions: { [key: string]: string[] };
    defaultsAdditions: { [key: string]: string };
  }
> = {
  id: "evaluateTableParams",
  questions: [],
  resolver: (answers) => ({}),
  validate: true,
};

export function tableToObject(
  table: Element | undefined | null
): { [key: string]: string }[] {
  if (table == null || table.tagName !== "TABLE") return [];
  const tableChildren = Array.from(table.children);
  const headerRow =
    tableChildren.find((el) => el.tagName === "THEAD")?.children[0] != null
      ? tableChildren.find((el) => el.tagName === "THEAD")?.children[0]
      : tableChildren.find((el) => el.tagName === "TBODY")?.children[0];
  const bodyRows =
    tableChildren.find((el) => el.tagName === "THEAD")?.children[0] != null
      ? Array.from(
          tableChildren.find((el) => el.tagName === "TBODY")?.children ?? []
        )
      : Array.from(
          tableChildren.find((el) => el.tagName === "TBODY")?.children ?? []
        ).slice(1);
  const keys =
    headerRow == null
      ? []
      : (Array.from(headerRow.children)
          .map((el) => el.textContent)
          .filter((el) => el != null) as string[]);
  const values = bodyRows.map(
    (row) =>
      Array.from(row.children)
        .map((child) => child.textContent)
        .filter((el) => el != null) as string[]
  );
  return values.map((row) =>
    Object.fromEntries(
      row.map((val, i) => [
        keys[i]
          .split(" ")
          .map((el, i) =>
            i == 0
              ? el.toLowerCase()
              : `${el[0].toUpperCase()}${el.slice(1).toLowerCase()}`
          )
          .join(""),
        val,
      ])
    )
  );
}

export async function initTable(page: puppeteer.Page) {
  const forceExportsType = (
    vartocheck: any
  ): vartocheck is { [key: string]: any } => true;
  if (!forceExportsType(exports)) return;
  return Promise.all(
    Object.entries(exports)
      .filter(([key, val]) => key !== "initTable" && typeof val == "function")
      .map(([key, val]: [string, Function]) =>
        page.addScriptTag({ content: val.toString() })
      )
  );
}
