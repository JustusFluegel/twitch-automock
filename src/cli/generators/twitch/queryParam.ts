import {
  ManualEvaluationPlaceholder,
  ManualEvaluationResolver,
} from "../../manualEvaluation";
import { table, getBorderCharacters } from "table";

import type {
  contentAfterHeaders as contentAfterHeadersI,
} from "./utils";
import type {
  tableToObject as tableToObjectI,
} from "./table";
declare const contentAfterHeaders: typeof contentAfterHeadersI;
declare const tableToObject: typeof tableToObjectI;
declare const Base64: {
  encode: (toEncode: string) => string;
  decode: (toDecode: string) => string;
};

type QueryParamTypes = 
| "integer"
| "integer[]"
| "double"
| "double[]"
| "float"
| "float[]"
| "long"
| "long[]"
| "string"
| "string[]"
| "object"
| "object[]"
| "boolean"
| "boolean[]"
| "custom";

export interface QueryParam {
  _interface: "QueryParam";
  parameter: string;
  description: string;
  required: boolean;
  type: QueryParamTypes,
  customText?: string
}

export function extractQueryParams(
  docsId: string,
  link: string
): (QueryParam | ManualEvaluationPlaceholder<QueryParam,"queryParam">)[] {
  const docs = document.getElementById(docsId);
  const tables = {
    params: tableToObject(
      contentAfterHeaders(["Query Parameters", "Query Parameter"], docs).find((el) => el.tagName === "TABLE")
    ),
    required: tableToObject(
      contentAfterHeaders(
        ["Required Query Parameter", "Required Query Parameters"],
        docs
      ).find((el) => el.tagName === "TABLE")
    ),
    optional: tableToObject(
      contentAfterHeaders(
        ["Optional Query Parameters", "Optional Query Parameter"],
        docs
      ).find((el) => el.tagName === "TABLE")
    ),
  };
  const allParams: [
    string,
    {
      required: string;
      type: string;
      description: string;
      original: { [key: string]: string };
    }
  ][] = Object.entries(tables)
    .map(([key, val]) =>
      val.map(
        (a) =>
          [
            (
              a["parameter"] ??
              a["paramater"] ??
              a["name"] ??
              a["param"]
            ).trim(),
            {
              type: (a["type"] ?? "STRING").toUpperCase().trim(),
              required: (
                a["required"] ??
                (key === "params" || key === "required" ? "Yes" : "No")
              )
                .toUpperCase()
                .trim(),
              description: a["description"]?.trim() ?? "",
              original: a,
            },
          ] as [
            string,
            {
              required: string;
              type: string;
              description: string;
              original: { [key: string]: string };
            }
          ]
      )
    )
    .reduce((a, b) => [...a, ...b]);

  const parsedParams: (
    | QueryParam
    | ManualEvaluationPlaceholder<QueryParam, "queryParam">
  )[] = [];
  for (const [
    parameter,
    { required, type, original, description },
  ] of allParams) {
    if (
      ![
        "INTEGER",
        "STRING",
        "BOOLEAN",
        "LONG",
        "DOUBLE",
        "FLOAT",
        "COMMA-SEPARATED LIST",
        "LIST OF INTEGERS",
      ].includes(type) ||
      (required !== "NO" && required !== "YES")
    )
      parsedParams.push({
        _interface: "ManualEvaluationPlaceholder",
        id: "queryParam",
        questionID: Base64.encode(JSON.stringify(original)),
        customData: {original: Object.keys(original).includes("required")
          ? original
          : { ...original, predicted_required: required }},
        header: "QueryParam",
        infoText: `The following table couldn't be automatically evaluated (see ${link}):`,
        preEvaluatedFields: {
          description,
          required:
            required === "YES" || required === "NO"
              ? required === "YES"
              : undefined,
          parameter
        },
      });
    else
      parsedParams.push({
        _interface: "QueryParam",
        required: required === "YES",
        parameter,
        description,
        type:
          type === "INTEGER"
            ? "integer"
            : type === "STRING"
            ? "string"
            : type === "BOOLEAN"
            ? "boolean"
            : type === "LONG"
            ? "long"
            : type === "DOUBLE"
            ? "double"
            : type === "FLOAT"
            ? "float"
            : type === "COMMA-SEPARATED LIST"
            ? "string[]"
            : type === "LIST OF INTEGERS"
            ? "integer[]"
            : "custom",
      });
  }
  return parsedParams;
}

export const isPlaceholder = (
  obj: ManualEvaluationPlaceholder<QueryParam,"queryParam"> | QueryParam
): obj is ManualEvaluationPlaceholder<QueryParam, "queryParam"> =>
  obj._interface === "ManualEvaluationPlaceholder" && obj.id === "queryParam";

export const resolver: ManualEvaluationResolver<QueryParam, "queryParam"> = {
  id: "queryParam",
  questions: [
    {
      identifier: "parameter",
      question: "Please enter the name of the parameter",
      answerType: "string",
    },
    {
      identifier: "description",
      question: "Please enter the description of the parameter",
      answerType: "string",
    },
    {
      identifier: "required",
      question: "Is the parameter required?",
      answerType: "boolean",
    },
    {
      identifier: "type",
      question: "Please choose the type of the parameter",
      answerType: "choices",
      choices: [
        "integer",
        "integer[]",
        "double",
        "double[]",
        "float",
        "float[]",
        "long",
        "long[]",
        "string",
        "string[]",
        "object",
        "object[]",
        "boolean",
        "boolean[]",
        "custom",
      ],
    },
    {
      identifier: "customDescription",
      question: "Please enter a custom description for the type when needed",
      answerType: "string"
    },
  ],
  customInfo: ({original}) =>
    console.log(
      table([Object.keys(original), Object.values(original)], {
        border: getBorderCharacters("honeywell"),
        columnDefault: { wrapWord: true },
        columns: Object.fromEntries(
          Object.entries(original).map(([key, val], i) => [
            i,
            {
              width:
                key.length > 50 || (val as string).length > 50
                  ? 50
                  : key.length > (val as string).length
                  ? key.length
                  : (val as string).length,
              wrapWord: true,
            },
          ])
        ),
      })
    ),
  resolver: (answers, customData) => ({
    _interface: "QueryParam",
    parameter: (answers.parameter as string).trim(),
    required: answers.required as boolean,
    description: (answers.description as string).trim(),
    type: answers.type as QueryParamTypes,
      customText: (answers.customDescription as string) == "" ? undefined:  (answers.customDescription as string)
  }),
  validate: true,
};
