import puppeteer from "puppeteer";

export function evaluationPlaceholderObjectInfoText(
  str: TemplateStringsArray,
  ...objects: any[]
) {
  let fullString = "";
  for (let i = 0; i < str.length + objects.length; i++) {
    if (i % 2 == 0) fullString += str[i / 2];
    else
      fullString += `\n ${JSON.stringify(objects[Math.floor(i / 2)], null, 4)}`;
  }
  return fullString;
}

export function contentAfterHeader(
  header: string,
  container: Element | null | undefined,
  headerTag: string = "H3"
): Element[] {
  if (container == null) return [];
  const startIndex = Array.from(container.children).findIndex(
    (val) =>
      val.tagName == headerTag.toUpperCase() &&
      header.trim().toUpperCase() ===
        (val.textContent?.trim().toUpperCase().replace(/\s/g, " ") ??
          "ifzogupohiüfasietgdühoaerghivvqüaerhdsüoqgadhsodahgfh")
  );
  if (startIndex == -1) return [];
  const afterStartIndex = Array.from(container.children).slice(startIndex + 1);
  const endIndex = afterStartIndex.some(
    (el) => el.tagName === headerTag.toUpperCase()
  )
    ? afterStartIndex.findIndex((el) => el.tagName == headerTag.toUpperCase())
    : afterStartIndex.length + 1;
  afterStartIndex
    .slice(0, endIndex)
    .forEach((el) => el.setAttribute("style", "display: none"));
  return afterStartIndex.slice(0, endIndex);
}
export function contentAfterHeaders(
  headers: string[],
  container: Element | null | undefined,
  headerTag: string = "H3"
): Element[] {
  return headers
    .map((header) => contentAfterHeader(header, container, headerTag))
    .reduce((a, b) => [...a, ...b], []);
}

export function contentUntilNextHeader(
  container: Element | null | undefined,
  headerTag: string = "H3"
): Element[] {
  if (container == null) return [];
  const endIndex = Array.from(container.children).findIndex(
    (el) => el.tagName == headerTag.toUpperCase()
  );
  Array.from(container.children)
    .slice(1, endIndex)
    .forEach((el) => el.setAttribute("style", "display: none"));
  if (endIndex === -1) return Array.from(container.children);
  else return Array.from(container.children).slice(1, endIndex);
}



export async function initUtils(page: puppeteer.Page) {
  const forceExportsType = (
    vartocheck: any
  ): vartocheck is { [key: string]: any } => true;
  if (!forceExportsType(exports)) return;
  return Promise.all(
    Object.entries(exports)
      .filter(([key, val]) => key !== "initUtils" && typeof val == "function")
      .map(([key, val]: [string, Function]) =>
        page.addScriptTag({ content: val.toString() })
      )
  );
}
