import type {
  contentAfterHeaders as contentAfterHeadersI,
} from "./utils";
import type {
  tableToObject as tableToObjectI
} from "./table";
declare const contentAfterHeaders: typeof contentAfterHeadersI;
declare const tableToObject: typeof tableToObjectI;
declare const Base64: {
  encode: (toEncode: string) => string;
  decode: (toDecode: string) => string;
};

export interface ResponseCodes {
  [key: string]: string;
}

export function extractResponseCodes(
  docsId: string,
  link: string
): ResponseCodes {
  const docs = document.getElementById(docsId);
  const table = tableToObject(
    contentAfterHeaders(
        ["Response Codes","Possible Response Codes"],
        docs
      ).find(a => a.tagName === "TABLE")
  )
   return Object.fromEntries(table.map(row => ([row["httpCode"] ?? row["code"] ?? "200", row["meaning"] ?? row["description"] ?? ""])));
}
