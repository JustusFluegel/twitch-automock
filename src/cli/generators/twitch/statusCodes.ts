  
  import type {
    contentAfterHeaders as contentAfterHeadersI,
  } from "./utils";
  import type {
    tableToObject as tableToObjectI
  } from "./table";
  declare const contentAfterHeaders: typeof contentAfterHeadersI;
  declare const tableToObject: typeof tableToObjectI;
  
  export interface StatusCodes {
    [key: string]: string;
  }
  
  export function extractStatusCodes(
    docsId: string,
    link: string
  ): StatusCodes {
    const docs = document.getElementById(docsId);
    const table = tableToObject(
      contentAfterHeaders(
          ["Code Statuses"],
          docs
        ).find(a => a.tagName === "TABLE")
    )
     return Object.fromEntries(table.map(row => ([row["httpCode"] ?? row["code"] ?? "200", row["meaning"] ?? row["description"] ?? ""])));
  }
  