import type { contentAfterHeaders as contentAfterHeadersI } from "./utils";
declare const contentAfterHeaders: typeof contentAfterHeadersI;

export function parseURL(
  baseUrl: string
): (
  url: string
) => {
  method: string;
  url: string;
  queryParams?: { [key: string]: string };
  urlParams?: { [key: string]: string };
} {
  return (rawUrl) => {
    const URL_REGEX = /^(?<method>GET|POST|PUT|PATCH|DELETE)? *["']*(?:(?<url>(?:http:\/\/|ws:\/\/|https:\/\/|wss:\/\/)[\S\s]+?)|(?<bareurl>[\S\s]+))(?:\?(?<queryParams>[\S\s]+?))?['"]*$/gim;
    const URLPARAMS_REGEX = /(?<=\/ *< *)[^\n]+?(?= *>(?: *\/| *$))/g;
    const URLPARAMS_REPLACER_REGEX = /(?<=\/) *<[^\n]+?> *(?=\/|$)/g;
    const groups = URL_REGEX.exec(rawUrl)?.groups;
    const url =
      groups?.url ??
      (groups?.bareurl != undefined
        ? `${baseUrl}${groups.bareurl.startsWith("/") ? "" : "/"}${
            groups.bareurl
          }`
        : "");
    const queryParams =
      groups?.queryParams
        ?.split("&")
        ?.map((el) =>
          ((el.trim() == "" ? null : el)?.split("=")?.length ?? 0) == 1
            ? [el, "boolean"]
            : (([key, val]) => [
                key,
                (val.match(/(?<= *<)[\s\S]+(?=> *)/g) ?? [val])[0],
              ])((el ?? "").split("="))
        ) ?? [];
    const urlParams = Object.fromEntries(
      (url.match(URLPARAMS_REGEX) ?? [])
        .map((match) => match.replace(/(?:^\s*\/*\s*<\s*|\s*>\s*\/*\s*$)/g, ""))
        .map((param) => [
          param
            .split(" ")
            .map((el, index) =>
              index == 0 ? el : `${el[0].toUpperCase()}${el.slice(1)}`
            )
            .join(""),
          param,
        ])
    );

    return {
      method: groups?.method ?? "GET",
      url: url
        .split(URLPARAMS_REPLACER_REGEX)
        .reduce(
          (a, b, index) =>
            `${a}{{${Object.keys(urlParams)[index - 1] ?? ""}}}${b}`
        ),
      queryParams: Object.fromEntries(queryParams),
      urlParams: Object.keys(urlParams).length > 0?  urlParams: undefined,
    };
  };
}

export function extractURLs(docsId: string): string[] {
  const docs = document.getElementById(docsId);
  return Array.from(
    contentAfterHeaders(["URL", "URLs"], docs)?.[0].children ?? []
  )
    .map((el) => el.textContent)
    .filter((text) => text != null && text.trim() != "") as string[];
}
