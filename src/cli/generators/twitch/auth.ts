import {
  ManualEvaluationPlaceholder,
  ManualEvaluationResolver,
} from "../../manualEvaluation";
import type { contentAfterHeaders as contentAfterHeadersI } from "./utils";
declare const contentAfterHeaders: typeof contentAfterHeadersI;
declare const Base64: {
  encode: (toEncode: string) => string;
  decode: (toDecode: string) => string;
};

export interface EndpointAuth {
  _interface: "EndpointAuth";
  oauth: {
    required: boolean;
    scopes?: string[];
    app?: boolean;
    user?: boolean;
  };
  onlySpecialArrangement: boolean;
  onlyBroadcaster: boolean;
  special: boolean;
  specialInfo?: string
}

export function extractAuth(
  docsId: string,
  link: string
): EndpointAuth | ManualEvaluationPlaceholder<EndpointAuth, "auth"> {
  const docs = document.getElementById(docsId);
  const elements = contentAfterHeaders(
    ["Authorization", "Authentication"],
    docs
  );
  const allText = elements?.map((el) => el.textContent).join("") ?? "";
  const allElements = elements
    ?.map((el) =>
      el.tagName == "UL"
        ? Array.from(el.children).map((el) => el.textContent)
        : [el.textContent]
    )
    .reduce((a, b) => [...a, ...b])
    .filter((el) => el != null) as string[];
  if (allText.toUpperCase() == "NONE")
    return {
      _interface: "EndpointAuth",
      onlyBroadcaster: false,
      onlySpecialArrangement: false,
      special: false,
      oauth: { required: false, },
    };

  const scopes: string[] = [];
  let app = false;
  let user = true;
  let required = false;
  let onlyBroadcaster = false;
  const leftoverElements = [];
  allElements.forEach((element) => {
    const scopeMatch = /^\s*(?:Required|Requires)\s+(?:OAuth\s+)?Scope:\s+(?<scope>[\w:]+)+\s*$/gim.exec(
      element
    )?.groups?.scope;
    const trimmedAndUppercase = element
      .trim()
      .toUpperCase()
      .replace(".", "")
      .replace(",", "")
      .replace(/\s+/g, " ");
    if (scopeMatch != null) scopes.push(scopeMatch);
    else if (
      trimmedAndUppercase === "OAUTH TOKEN REQUIRED" ||
      trimmedAndUppercase === "USER OAUTH TOKEN"
    ) {
      // Default values are fine, just keep them, so do nothing
    } else if (
      trimmedAndUppercase === "OAUTH OR APP ACCESS TOKEN REQUIRED" ||
      trimmedAndUppercase === "VALID USER TOKEN OR APP ACCESS TOKEN" ||
      trimmedAndUppercase === "USER OAUTH TOKEN OR APP ACCESS TOKEN"
    ) {
      app = true;
    } else if (
      trimmedAndUppercase ===
        "QUERY PARAMETER BROADCASTER_ID MUST MATCH THE USER_ID IN THE USER ACCESS TOKEN" ||
      trimmedAndUppercase ===
        "QUERY PARAMETER BROADCASTER_ID MUST MATCH THE USER_ID IN THE USER-ACCESS TOKEN" ||
      trimmedAndUppercase ===
        "BROADCASTERS CAN ONLY REQUEST THEIR OWN SUBSCRIPTIONS"
    ) {
      onlyBroadcaster = true;
      app = false;
    } else if (
      trimmedAndUppercase === "APP ACCESS TOKEN REQUIRED" ||
      trimmedAndUppercase === "APP ACCESS OAUTH TOKEN REQUIRED" ||
      trimmedAndUppercase == "APP ACCESS TOKEN"
    ) {
      app = true;
      user = false;
    } else {
      leftoverElements.push(element);
    }
  });

  if (leftoverElements.length === 0)
    return {
      _interface: "EndpointAuth",
      oauth: {
        required: required || app,
        app,
        user,
        scopes: scopes.length == 0 ? undefined: scopes,
      },
      onlySpecialArrangement: false,
      special: false,
      onlyBroadcaster,
    };

  return {
    _interface: "ManualEvaluationPlaceholder",
    id: "auth",
    questionID: Base64.encode(JSON.stringify(allElements)),
    header: "Authentication",
    infoText: `The following Auth sections couldn't be automatically evaluated (see ${link}):\n\n ${allElements
      .map((el) => `\t${el}\n`)
      .join("")}`,
      customData: {
        rawData: allElements
      },
    preEvaluatedFields: {
      oauthRequired: required,
      oauthUser: user,
      oauthApp: app,
      oauthScopes: scopes,
      onlyBroadcaster,
      special: true,
    },
  };
}

export const isPlaceholder = (
  obj: ManualEvaluationPlaceholder<EndpointAuth, "auth"> | EndpointAuth
): obj is ManualEvaluationPlaceholder<EndpointAuth,"auth"> =>
  obj._interface === "ManualEvaluationPlaceholder";

export const resolver: ManualEvaluationResolver<EndpointAuth, "auth"> = {
  id: "auth",
  questions: [
    {
      identifier: "oauthRequired",
      question: "Do you need a Token for the endpoint?",
      answerType: "boolean",
    },
    {
      identifier: "oauthUser",
      question:
        'Can you use a user token for the endpoint? ( mentioned when you can\'t, eg. "App Token Required" )',
      answerType: "boolean",
    },
    {
      identifier: "oauthApp",
      question:
        "Can you use a app token for the endpoint? ( mentioned when you can )",
      answerType: "boolean",
    },
    {
      identifier: "oauthScopes",
      question:
        'Please enter the required scopes seperated by a ",", and prepend optional ones with a ?.',
      answerType: "string[]",
    },
    {
      identifier: "onlyBroadcaster",
      question:
        "Is the endpoint only available if you have a broadcaster token?",
      answerType: "boolean",
    },
    {
      identifier: "onlySpecialArrangement",
      question:
        "Do you need a special arrangement with Twitch for the endpoint?",
      answerType: "boolean",
    },
    {
      identifier: "special",
      question: "Is this endpoint in any way special?",
      answerType: "boolean",
    },
  ],
  resolver: (answers, customData) => ({
    _interface: "EndpointAuth",
    oauth: {
      required: answers.oauthRequired as boolean,
      scopes: ((answers.oauthScopes as string[] | undefined)?.length ?? 0) > 0 ? answers.oauthScopes as string[]: undefined ,
      app: answers.oauthApp as boolean,
      user: answers.oauthUser as boolean,
    },
    onlyBroadcaster: answers.onlyBroadcaster as boolean,
    onlySpecialArrangement: answers.onlySpecialArrangement as boolean,
    special: answers.special as boolean,
    specialInfo: (answers.special as boolean) ? customData.rawData.join("\n").trim(): undefined
  }),
  validate: true,
};
