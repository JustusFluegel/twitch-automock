import { v4 as uuidv4 } from "uuid";
import puppeteer from "puppeteer";

export interface IDs {
  docsId: string;
  parentId: string;
  codeId: string;
}

export function assignIDs(
  selector: string,
  docsId: string,
  parentId: string,
  codeId: string
) {
  const docs = document.querySelector(selector)?.parentElement as
    | Element
    | null
    | undefined;
  const parent = docs?.parentElement;
  const code = parent?.children[1];
  if (docs != null) docs.setAttribute("id", `${docs.id} ${docsId}`.trim());
  if (parent != null)
    parent.setAttribute("id", `${parent.id} ${parentId}`.trim());
  if (code != null) code.setAttribute("id", `${code.id} ${codeId}`.trim());
  return { docsId, parentId, codeId };
}

export async function puppeteerAssignIDs(
  page: puppeteer.Page,
  selector: string
) {
  return (await page.evaluate(
    assignIDs,
    selector,
    `puppeteerrandomid_docs_${uuidv4()}`,
    `puppeteerrandomid_parent_${uuidv4()}`,
    `puppeteerrandomid_code_${uuidv4()}`
  )) as IDs;
}
