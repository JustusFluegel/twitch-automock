import puppeteer from "puppeteer";
import fs from "fs";

import { startCooldown } from "../startCooldown";
import { extractURLs, parseURL } from "./urls";
import { puppeteerAssignIDs, IDs } from "./ids";
import * as auth from "./auth";
import * as queryParam from "./queryParam";
import {
  initUtils,
  contentUntilNextHeader as contentUntilNextHeaderI,
} from "./utils";
import {
  initTable,
} from "./table";
declare const contentUntilNextHeader: typeof contentUntilNextHeaderI;
import { ManualEvaluation } from "../../manualEvaluation";
import { extractResponseCodes } from "./responseCodes";
import { extractStatusCodes } from "./statusCodes";

const MCKSPEC_VERSION = "1";

const DEBUG = process.env["DEBUG"] == "true";
const KEEP_PAGES = DEBUG && process.env["KEEP_PAGES"] != "false";
const ONLY_FIRST = DEBUG && process.env["ONLY_FIRST"] == "true";
const DEVTOOLS = process.env["DEVTOOLS"] == "true";
const START_COOLDOWN = parseInt(process.env["START_COOLDOWN"] ?? "20000");

enum ReferenceType {
  APIDocs,
}

const REFERENCES: {
  api: string;
  url: string;
  type: ReferenceType;
  selector: string;
  tableFormat: string[];
  tableOverrides?: { [key: string]: any };
  baseUrl: string;
}[] = [
  {
    api: "helix",
    url: "https://dev.twitch.tv/docs/api/reference",
    type: ReferenceType.APIDocs,
    selector: "body > div.main > section:nth-child(1) > section > table ",
    tableFormat: ["resource", "endpoint", "description"],
    baseUrl: "https://api.twitch.tv",
  },
  ...[
    "Bits",
    "Channels",
    "Chat",
    "Clips",
    "Collections",
    "Games",
    "Ingests",
    "Search",
    "Streams",
    "Teams",
    "Users",
    "Videos",
  ].map((el) => ({
    api: "kraken",
    url: `https://dev.twitch.tv/docs/v5/reference/${el.toLowerCase()}`,
    type: ReferenceType.APIDocs,
    selector: "body > div.main > section:nth-child(1) > section > table",
    tableFormat: ["endpoint", "description"],
    tableOverrides: { resource: el },
    baseUrl: "https://api.twitch.tv",
  })),
].filter((_, index) => index == 0 || !ONLY_FIRST);

export async function generate(
  outFile: string,
  manualEvaluationSaveFile: string | null,
  manualEvaluationFile: string | null,
  skipValidation: boolean
) {
  if (DEBUG) await startCooldown(START_COOLDOWN);

  const manualEvaluation = new ManualEvaluation(skipValidation);
  manualEvaluation.addResolver(auth.resolver);
  manualEvaluation.addResolver(queryParam.resolver);
  if (manualEvaluationFile != null)
    await manualEvaluation.loadFrom(manualEvaluationFile);

  const browser = await puppeteer.launch({
    headless: !DEBUG,
    defaultViewport: { width: 1200, height: 800 },
    devtools: DEBUG && DEVTOOLS,
  });

  interface Link {
    id: string;
    endpoint: string;
    [key: string]: string;
  }

  const labeledUrls: { [key: string]: string[] } = {};
  REFERENCES.forEach(({ api, url }) => {
    if (labeledUrls[api] == null) labeledUrls[api] = [];
    labeledUrls[api][labeledUrls[api].length] = url;
  });

  const all: { [key: string]: { [key: string]: any }[] } = {};

  console.log("This will take a while. Please Wait.");

  for (const {
    api,
    url,
    selector,
    baseUrl,
    tableFormat,
    type,
    tableOverrides,
  } of REFERENCES) {
    const page = await browser.newPage();
    await page.goto(url);
    await page.addScriptTag({
      url: "https://cdn.jsdelivr.net/npm/js-base64@3.6.0/base64.min.js",
    });
    await initUtils(page);
    await initTable(page);
    const links: Link[] =
      (await page.evaluate(
        (
          selector: string,
          tableFormat: string[],
          tableOverrides: { [key: string]: any }
        ): Link[] | null => {
          const el = document.querySelector(selector);
          if (el != null)
            return Array.from(el.children).map((child) => ({
              ...Object.fromEntries(
                tableFormat.map((label, index) => [
                  label,
                  child.children[index].textContent,
                ])
              ),
              ...tableOverrides,
              endpoint:
                child.children[tableFormat.indexOf("endpoint")].textContent ??
                "",
              id:
                child.children[
                  tableFormat.indexOf("endpoint")
                ].children[0].getAttribute("href") ?? "",
            }));
          return null;
        },
        `${selector} > tbody`,
        tableFormat,
        tableOverrides ?? {}
      )) ?? [];

    const pageEntries: { [key: string]: any } = [];
    for (const { id, ...remaining } of links) {
      if (DEBUG) await page.$eval(id, (el) => el.scrollIntoView());

      const { docsId, parentId, codeId }: IDs = await puppeteerAssignIDs(
        page,
        id
      );

      await page.evaluate((codeId) => {
        document.getElementById(codeId)?.setAttribute("style", "display: none");
      }, codeId);

      const urls: {
        method: string;
        url: string;
        queryParams?: { [key: string]: string };
        urlParams?: { [key: string]: string };
      }[] = (await page.evaluate(extractURLs, docsId)).map(parseURL(baseUrl));

      const tempAuthField = await page.evaluate(
        auth.extractAuth,
        docsId,
        `${url}${id}`
      );

      const authField: auth.EndpointAuth = auth.isPlaceholder(tempAuthField)
        ? await manualEvaluation.evaluate(tempAuthField)
        : tempAuthField;

      const longDescription = await page.evaluate(
        (docsId) =>
          contentUntilNextHeader(document.getElementById(docsId))
            ?.filter((el) => !el.classList.contains("editor-link"))
            ?.reduce((a, b) => `${a}${b.textContent}`, "") ?? "",
        docsId
      );

      const tempQueryParams = await page.evaluate(
        queryParam.extractQueryParams,
        docsId,
        `${url}${id}`
      );
      const queryParams: queryParam.QueryParam[] = [];

      for (const param of tempQueryParams) {
        queryParams.push(
          queryParam.isPlaceholder(param)
            ? await manualEvaluation.evaluate(param)
            : param
        );
      }

      const responseCodes = await page.evaluate(extractResponseCodes, docsId);
      const statusCodes = await page.evaluate(extractStatusCodes, docsId);

      pageEntries[pageEntries.length] = {
        ...remaining,
        label: api,
        longDescription,
        urls: urls.map((url) => {const qParams = queryParams.map((param) => ({
          ...param,
          _interface: undefined,
        }));return({
          ...url,
          queryParams: qParams.length > 0? qParams: undefined
        });}),
        auth: { ...authField, _interface: undefined },
        responseCodes:
          Object.keys(responseCodes).length > 0 ? responseCodes : undefined,
        statusCodes:
          Object.keys(statusCodes).length > 0 ? statusCodes : undefined,
      };
    }
    if (all[api] == null) all[api] = [];
    pageEntries.forEach(
      (entry: { [key: string]: any }) => (all[api][all[api].length] = entry)
    );
    if (!KEEP_PAGES) await page.close();
  }

  await fs.promises.writeFile(
    outFile,
    JSON.stringify({ version: MCKSPEC_VERSION, spec: all })
  );
  if (!KEEP_PAGES) await browser.close();
  if (manualEvaluationSaveFile != null)
    await manualEvaluation.saveTo(manualEvaluationSaveFile);
}
