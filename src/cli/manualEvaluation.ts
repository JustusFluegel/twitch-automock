import { prompt } from "enquirer";
import fs from "fs";
import figlet from "figlet";

export interface ManualEvaluationResolver<T extends { [key: string]: any }, ID extends string, customDataT extends object | null = null > {
  id: ID;
  header?: string;
  questions: {
    question: string;
    answerType: "string" | "number" | "boolean" | "string[]" | "choices";
    identifier: string;
    choices?: string[];
  }[];
  customInfo?: (options: any) => void | Promise<void>;
  resolver: (
    answers: {
      [identifier: string]: string | number | boolean | string[];
    },
    customData: customDataT
  ) => T | Promise<T>;
  validate: boolean;
  valid?: <T>(object: T) => boolean | Promise<boolean>;
}

export interface ManualEvaluationPlaceholder<ID extends string, customDataT extends object | null = null > {
  _interface: "ManualEvaluationPlaceholder";
  customData: customDataT;
  id: ID;
  questionID: string;
  header?: string;
  infoText?: string;
  preEvaluatedFields?: {
    [key: string]: string | number | boolean | string[] | undefined;
  };
}

export class ManualEvaluation {
  #lastHeader: string = "";
  instances: ManualEvaluationResolver<any, any, object | null>[] = [];
  solved: {
    [resolverID: string]: {
      [questionID: string]: {
        [answerId: string]: string | number | boolean | string[];
      };
    };
  } = {};
  skipValidation: boolean;

  constructor(skipValidation: boolean = false) {
    this.skipValidation = skipValidation;
  }

  addResolver<T,ID extends string>(resolver: ManualEvaluationResolver<T, ID, object | null>) {
    this.instances.push(resolver);
  }

  async loadFrom(file: string) {
    this.solved = {
      ...this.solved,
      ...JSON.parse(await fs.promises.readFile(file, "utf-8")),
    };
  }

  async saveTo(file: string) {
    await fs.promises.writeFile(file, JSON.stringify(this.solved, null, 4));
  }

  async evaluate<T, ID extends string, customDataT extends object | null = null>(placeholder: ManualEvaluationPlaceholder<ID, customDataT>): Promise<T> {
    const resolver:
      | ManualEvaluationResolver<T,ID, customDataT>
      | undefined = this.instances.find(
      (resolver) => resolver.id === placeholder.id
    );
    if (resolver === undefined)
      throw new Error(
        `ManualEvaluationResolver not found for id ${placeholder.id}`
      );

    if (this.solved[placeholder.id] == null) this.solved[placeholder.id] = {};
    if (this.solved[placeholder.id][placeholder.questionID] == null)
      this.solved[placeholder.id][placeholder.questionID] = {};

    let valid = false;
    let retries = 0;
    let resolvedObject: T | undefined = undefined;
    const allResultsProvided =
      Object.keys(this.solved[placeholder.id][placeholder.questionID])
        .length === resolver.questions.length;

    while (!valid) {
      if (retries != 0) console.log("\nPlease re-enter wrong values:");
      if(!(this.skipValidation && allResultsProvided))console.log("\n");
      if (
        !(this.skipValidation && allResultsProvided) &&
        retries === 0 &&
        (placeholder.header != null || resolver.header != null)
      ) {
        const header = placeholder.header ?? resolver.header ?? "";
        if (this.#lastHeader != header) console.log(figlet.textSync(header));
        this.#lastHeader = header;
      }
      if (
        !(this.skipValidation && allResultsProvided) &&
        retries % 10 === 0 &&
        placeholder.infoText != null
      )
        console.log(placeholder.infoText);
      if (
        !(this.skipValidation && allResultsProvided) &&
        retries % 10 === 0 &&
        resolver.customInfo != null
      )
        resolver.customInfo(placeholder.customData);
      const answers: {
        [identifier: string]: string | number | boolean | string[];
      } = {};

      for (const question of resolver.questions) {
        if (
          retries === 0 &&
          this.solved[placeholder.id][placeholder.questionID][
            question.identifier
          ] != null
        )
          answers[question.identifier] = this.solved[placeholder.id][
            placeholder.questionID
          ][question.identifier];
        else {
          const {
            result,
          }: {
            result: string | number | boolean | string[];
          } = await prompt({
            name: "result",
            message: question.question,
            type:
              question.answerType === "boolean"
                ? "confirm"
                : question.answerType === "string"
                ? "input"
                : question.answerType === "string[]"
                ? "list"
                : question.answerType === "choices"
                ? "select"
                : "numeral",
            choices:
              question.answerType === "choices" ? question.choices : undefined,
            initial: placeholder.preEvaluatedFields?.[question.identifier],
          } as any);
          const filteredResult =
            question.answerType === "string[]"
              ? (result as string[]).filter((a) => a !== "")
              : result;
          this.solved[placeholder.id][placeholder.questionID][
            question.identifier
          ] = filteredResult;
          answers[question.identifier] = filteredResult;
        }
      }

      resolvedObject = await resolver.resolver(answers, placeholder.customData);
      valid =
        (resolver.valid == null || (await resolver.valid(resolvedObject))) &&
        (!resolver.validate || this.skipValidation ||
          (await (async () => {
            console.log(resolvedObject);
            return ((await prompt({
              type: "confirm",
              name: "valid",
              message: "Is the above information valid?",
            })) as { valid: boolean }).valid;
          })()));
      retries++;
    }

    if (resolvedObject === undefined)
      throw new Error("This should never happen ...");
    return resolvedObject;
  }
}
